package AngajatiApp.model;
import org.junit.*;
import static AngajatiApp.controller.DidacticFunction.*;
import static org.junit.Assert.*;

public class EmployeeTest {

    private static Employee e1, e2;

    @Before
    public void setUp() throws Exception {
        e1 = new Employee("FN1","LN1","1111111111111",ASISTENT,12.12);
        e2 = new Employee("FN2","LN2","2222222222222",TEACHER,31.12);
    }

    @After
    public void tearDown() throws Exception {
        e1 = e2 = null;
    }

    @Test(expected = NullPointerException.class)
    public void testConstructor() {
        Employee e3 = new Employee();
        e3 = null;
        e3.getCnp();
    }

    @Test(timeout = 5000)
    public void getFirstName() {
    assertNotEquals("FN2",e1.getFirstName());
    }

    @Test
    public void setFirstName() {
    e1.setFirstName("FN2");
    assertEquals("FN2",e1.getFirstName());
    }

    @Test
    public void getLastName() {
    assertEquals("LN2",e2.getLastName());
    }

    @Test(timeout = 5000)
    public void setLastName() {
    e2.setLastName("LN1");
    assertEquals("LN1",e2.getLastName());
    }

    @Test
    public void getCnp() {
    assertNotEquals("2222222222222",e1.getCnp());
    }

    @Ignore
    @Test
    public void setCnp() {
    e1.setCnp("3333333333333");
    assertEquals("1111111111111",e1.getCnp());
    }

    @Ignore
    @Test
    public void getFunction() {
    assertEquals(ASISTENT,e1.getFunction());
    }

    @Test(timeout = 1500)
    public void setFunction() {
    e2.setFunction(ASISTENT);
    assertEquals(ASISTENT,e2.getFunction());
    }

    @Test
    public void getSalary() {
    assertEquals(12.12,e1.getSalary(),0);
    }

    @Test
    public void setSalary() {
    e1.setSalary(13.13);
        assertEquals(13.13,e1.getSalary(),0);
    }

}